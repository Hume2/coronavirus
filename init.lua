minetest.register_craftitem(":default:coronavirus", {
	description = "Coronavirus",
	inventory_image = "coronavirus.png",
	stack_max = 1000000,
})

minetest.register_craft({
	type = "shapeless",
	output = "default:coronavirus 2",
	recipe = {"default:coronavirus"},
})

